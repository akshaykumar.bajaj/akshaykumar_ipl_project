const fs = require('fs');

module.exports = function countWinnigperTeamperYear(matcehs_obj) {

    let winningPerTeamPerYear = {};

    for (let index = 0; index < matcehs_obj.length; index++) {
        let year = matcehs_obj[index].season;
        let curr_winner = matcehs_obj[index].winner;
        if (winningPerTeamPerYear[year] === undefined)
            winningPerTeamPerYear[year] = {};
        else if (winningPerTeamPerYear[year][curr_winner] === undefined)
            winningPerTeamPerYear[year][curr_winner] = 1;
        else
            winningPerTeamPerYear[year][curr_winner] += 1;
    }
     
    let output_path = 'src/public/output/WinningPerTeamPerYear.json';
    let result = JSON.stringify(winningPerTeamPerYear, '', 2);
    fs.writeFile(output_path, result, (err) => {
        if (err) {
            console.error(err);
        }
        console.log("Winning Per Team Per Year saved.");
    });
}
