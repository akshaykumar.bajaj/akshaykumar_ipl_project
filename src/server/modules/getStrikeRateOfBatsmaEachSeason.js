const fs = require('fs');

module.exports = function getStrikeRateOfBatsmanEachSeason(deliveries_object, match_ids_of_year) {
    let batsman = {};
    for (let year in match_ids_of_year) {
        for (let index in deliveries_object) {
            if (match_ids_of_year[year].includes(deliveries_object[index].match_id)) {
                let curr_batsman = deliveries_object[index].batsman;
                if (batsman[curr_batsman] === undefined)
                    batsman[curr_batsman] = {} 
                else if(batsman[curr_batsman][year] === undefined)
                batsman[curr_batsman][year] = {'runs': parseInt(deliveries_object[index].batsman_runs), 'balls': 1 }
                else
                {
                    batsman[curr_batsman][year].runs += parseInt(deliveries_object[index].batsman_runs);
                    batsman[curr_batsman][year].balls += 1;
                }
            }
        }
    }

    for (let curr_batsman in batsman){
        for(let curr_year in batsman[curr_batsman]){
            let strikerate = (batsman[curr_batsman][curr_year].runs/batsman[curr_batsman][curr_year].balls)*100;
            batsman[curr_batsman][curr_year] = parseFloat(strikerate.toFixed(2));
        }
    } 

    let result = JSON.stringify(batsman, '', 2);
    let output_path = 'src/public/output/batsmanstrikerate.json';
    fs.writeFile(output_path, result, (err) => {
        if (err) {
            console.error(err);
        }
        console.log("batsman strikerate per year saved.");
    });

}