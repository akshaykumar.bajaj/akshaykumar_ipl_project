const fs = require('fs');

module.exports = function getPlayerWithHighestPOMAwardsofEachYear(matches_obj) {
    let player_of_match_awards = {};
    let player_with_highest_pom_awards_of_each_year = {};

    for (let index = 0; index < matches_obj.length; index++) {
        let year = matches_obj[index].season;
        let curr_player_of_match = matches_obj[index].player_of_match;

        if (player_of_match_awards[year] === undefined)
            player_of_match_awards[year] = {};

        else if (player_of_match_awards[year][curr_player_of_match] === undefined)
            player_of_match_awards[year][curr_player_of_match] = 1;
        else
            player_of_match_awards[year][curr_player_of_match] += 1;
    }

    for (let index in player_of_match_awards) {
        let pom_array = Object.entries(player_of_match_awards[index]);
        pom_array.sort((value1, value2) => value2[1] - value1[1]);
        player_with_highest_pom_awards_of_each_year[index] = pom_array[0][0];
    }   

    let result = JSON.stringify(player_with_highest_pom_awards_of_each_year, '', 2);
    let output_path = 'src/public/output/PlayerWithHighestPOMAwards.json';
    fs.writeFile(output_path, result, (err) => {
        if (err) {
            console.error(err);
        }
        console.log("PlayerWithHighestPOMAwards per year saved.");
    });
}