const fs = require('fs');

module.exports = function economicalBolwers(deliveries_object, match_ids_of_year) {
    let bowlers = {};
    for (let index in deliveries_object) {
        if (match_ids_of_year.includes(deliveries_object[index].match_id)) {
            let curr_bowler = deliveries_object[index].bowler;
            if (bowlers[curr_bowler] == undefined)
                bowlers[curr_bowler] = { 'runs': parseInt(deliveries_object[index].total_runs), 'balls': 1 };
            else {
                bowlers[curr_bowler].runs += parseInt(deliveries_object[index].total_runs);
                bowlers[curr_bowler].balls += 1;
            }
        }
    }

    let bowlers_economy = [];
    for (let index in bowlers) {
        let curr_bowler_economy = bowlers[index].runs * 6 / bowlers[index].balls;
        bowlers[index].economy = parseFloat(curr_bowler_economy.toFixed(2));
        bowlers_economy.push({ 'name': index, 'economy': bowlers[index].economy });
    }

    bowlers_economy.sort((firstValue, secondValue) => {
        return  firstValue.economy - secondValue.economy;
    })

    let result = JSON.stringify(bowlers_economy.slice(0, 10), '', 2);
    let output_path = 'src/public/output/top10bowlersbyEconomy.json';
    fs.writeFile(output_path, result, (err) => {
        if (err) {
            console.error(err);
        }
        console.log("Top 10 bowlers by economy saved.");
    });

}

