const fs = require('fs');

module.exports = function countWinningTossAsWellAsMatchPerTeamPerYear(matcehs_obj) {

    let tossAsWellAsMatchWonperTeamperYear = {};

    for (let index = 0; index < matcehs_obj.length; index++) {
        let year = matcehs_obj[index].season;
        let curr_winner = matcehs_obj[index].winner;
        let curr__toss_winner = matcehs_obj[index].toss_winner;
        if(curr__toss_winner === curr_winner)
        {
            if (tossAsWellAsMatchWonperTeamperYear[year] === undefined)
                tossAsWellAsMatchWonperTeamperYear[year] = {};
            else if (tossAsWellAsMatchWonperTeamperYear[year][curr_winner] === undefined)
                tossAsWellAsMatchWonperTeamperYear[year][curr_winner] = 1;
            else
                tossAsWellAsMatchWonperTeamperYear[year][curr_winner] += 1;
        }
    }

    let output_path = 'src/public/output/WinningTossAsWellAsMatchPerTeamPerYear.json';
    let result = JSON.stringify(tossAsWellAsMatchWonperTeamperYear, '', 2);
    fs.writeFile(output_path, result, (err) => {
        if (err) {
            console.error(err);
        }
        console.log("The number of times each team won the toss and also won the match saved.");
    });
}
