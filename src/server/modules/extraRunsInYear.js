const fs = require('fs');

module.exports = function extraRunsInYear(deliveries_object, match_ids_of_year){
    let extraRunsperTeam = {};
    for (let index in deliveries_object) {
        if (match_ids_of_year.includes(deliveries_object[index].match_id)) {
            let team = deliveries_object[index].bowling_team
            if (extraRunsperTeam[team] == undefined)
                extraRunsperTeam[team] = parseInt(deliveries_object[index].extra_runs);
            else
                extraRunsperTeam[team] += parseInt(deliveries_object[index].extra_runs);
        }
    }

    let result = JSON.stringify(extraRunsperTeam, '', 2);
    let output_path = 'src/public/output/extraRunsPerTeam.json';
    fs.writeFile(output_path, result, (err) => {
        if (err) {
            console.error(err);
        }
        console.log("extra Runs Per Team saved.");
    });
}

