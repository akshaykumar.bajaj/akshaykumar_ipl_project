const fs = require('fs');

module.exports = function countMatchesPerYear(matches_obj) {
    let matches_per_year = {};

    for (let index = 0; index < matches_obj.length; index++) {
        let year = matches_obj[index].season;
        if (matches_per_year[year] === undefined)
            matches_per_year[year] = 1;
        else
            matches_per_year[year] += 1;
    }

    let result = JSON.stringify(matches_per_year, '', 2);
    let output_path = 'src/public/output/matchesPerYear.json';
    fs.writeFile(output_path, result, (err) => {
        if (err) {
            console.error(err);
        }
        console.log("Matches per year saved.");
    });
}
