const fs = require('fs');

module.exports = function economicalBowlersInSuperOvers(deliveries_object) {
    let superover_bowlers = {};
    for (let index in deliveries_object) {
        if (deliveries_object[index].is_super_over === '1') {
            let curr_bowler = deliveries_object[index].bowler;
            if (superover_bowlers[curr_bowler] == undefined)
                superover_bowlers[curr_bowler] = { 'runs': parseInt(deliveries_object[index].total_runs), 'balls': 1 };
            else {
                superover_bowlers[curr_bowler].runs += parseInt(deliveries_object[index].total_runs);
                superover_bowlers[curr_bowler].balls += 1;
            }
        }
    }

    let superover_bowlers_economy = [];
    for (let index in superover_bowlers) {
        superover_bowlers[index].economy = superover_bowlers[index].runs * 6 / superover_bowlers[index].balls;
        superover_bowlers_economy.push({ 'name': index, 'economy': superover_bowlers[index].economy });
    }

    superover_bowlers_economy.sort((firstValue, secondValue) => {
        return firstValue.economy - secondValue.economy;
    })

    let result = JSON.stringify(superover_bowlers_economy[0]);
    let output_path = 'src/public/output/topSuperoverEconomybowler.json';
    fs.writeFile(output_path, result, (err) => {
        if (err) {
            console.error(err);
        }
        console.log("Top super over bowler by economy saved.");
    });

}

