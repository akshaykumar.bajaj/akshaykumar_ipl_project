const fs = require('fs');

module.exports = function getMatchIdsofYear(matches_obj, year) {
        let match_ids_per_year = {};
          
    for (let index = 0; index < matches_obj.length; index++) {
        let year = matches_obj[index].season;
        let id = matches_obj[index].id;
        if (match_ids_per_year[year] === undefined)
            match_ids_per_year[year] = [];
            
            match_ids_per_year[year].push(id)
    }
    return match_ids_per_year;
}