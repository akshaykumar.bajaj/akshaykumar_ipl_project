let getMatchIdsofYear = require('./modules/getMatchIdsofYear.js')
let extraRunsInYear = require('./modules/extraRunsInYear.js');
let countMatchesPerYear = require('./modules/countMatchesPerYear.js');
let countWinnigperTeamperYear = require('./modules/countWinnigperTeamperYear.js');
let economicalBolwers = require('./modules/economicalBowlersInYear.js')
let countWinningTossAsWellAsMatchPerTeamPerYear = require('./modules/countWinningTossAsWellAsMatchPerTeamPerYear.js');
let getPlayerWithHighestPOMAwardsofEachYear = require('./modules/getPlayerWithHighestPlayerOfTheMatchAwardsofEachYear.js');
let getStrikeRateOfBatsmaEachSeason = require('./modules/getStrikeRateOfBatsmaEachSeason');
let economicalBowlersInSuperOvers = require('./modules/economicalBowlersInSuperOvers');

module.exports = {
    getMatchIdsofYear,
    extraRunsInYear,
    countMatchesPerYear,
    countWinnigperTeamperYear,
    economicalBolwers,
    countWinningTossAsWellAsMatchPerTeamPerYear,
    getPlayerWithHighestPOMAwardsofEachYear,
    getStrikeRateOfBatsmaEachSeason,
    economicalBowlersInSuperOvers,
}