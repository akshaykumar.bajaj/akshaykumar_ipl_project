let csv = require('csvtojson');
let ipl = require('./ipl.js');

let matches_csv = 'src/data/matches.csv';
let deliveries_csv = 'src/data/deliveries.csv';

//1.Number of matches played per year for all the years in IPL.
csv().fromFile(matches_csv)
    .then((matches_object) => ipl.countMatchesPerYear(matches_object));

//2.Number of matches won per team per year in IPL.
csv().fromFile(matches_csv)
    .then((matches_object) => ipl.countWinnigperTeamperYear(matches_object));

//3.Extra runs conceded per team in the year 2016
csv().fromFile(matches_csv)
    .then((matches_object) => ipl.getMatchIdsofYear(matches_object))
    .then((match_ids_of_year) => {
        csv().fromFile(deliveries_csv)
            .then((deliveries_obj) => ipl.extraRunsInYear(deliveries_obj, match_ids_of_year[2016]))
    });

//4.Top 10 economical bowlers in the year 2015
csv().fromFile(matches_csv)
    .then((matches_object) => ipl.getMatchIdsofYear(matches_object))
    .then((match_ids_of_year) => {
        csv().fromFile(deliveries_csv)
            .then((deliveries_obj) => ipl.economicalBolwers(deliveries_obj, match_ids_of_year[2015]))
    });


//5.Find the number of times each team won the toss and also won the match
csv().fromFile(matches_csv)
    .then((matches_object) => ipl.countWinningTossAsWellAsMatchPerTeamPerYear(matches_object));

//6.Find a player who has won the highest number of Player of the Match awards for each season
csv().fromFile(matches_csv)
    .then((matches_object) => ipl.getPlayerWithHighestPOMAwardsofEachYear(matches_object));

//7.Find the strike rate of a batsman for each season
csv().fromFile(matches_csv)
    .then((matches_object) => ipl.getMatchIdsofYear(matches_object))
    .then((match_ids_of_year) => {
        csv().fromFile(deliveries_csv)
            .then((deliveries_obj) => ipl.getStrikeRateOfBatsmaEachSeason(deliveries_obj, match_ids_of_year))
    });
//8.
csv().fromFile(deliveries_csv)
            .then((deliveries_obj) => ipl.economicalBowlersInSuperOvers(deliveries_obj));
