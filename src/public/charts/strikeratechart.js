fetch("/output/batsmanstrikerate.json")
    .then(response => response.json())
    .then(json => {
        let batsman_names = Object.keys(json);
        let years = ['2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017'];
        let series_array = [];
        batsman_names.forEach(batsman => {
            let year_played = Object.keys(json[batsman]);
            let strikerate = [];
            years.forEach(year => {
                if (year_played.includes(year))
                    strikerate.push(json[batsman][year]);
                else
                    strikerate.push(null);
            })
            series_array.push({visible: false,name : batsman , data: strikerate});
        })

        Highcharts.chart('batsmanstrikerate', {
             chart:{type: 'spline',zoomType: "x",},

            title: {
                text: 'Batsman Strike Rate Over Year'
            },
            subtitle: {
                text: 'Select batman to see graph'
            },

            xAxis: {
                title: {
                    text: 'YEARS'
                },

                categories: years,
            },

            yAxis: {
                title: {
                    text: 'STRIKERATE'
                },
            },

            series: series_array,
            
        }) 
    })
