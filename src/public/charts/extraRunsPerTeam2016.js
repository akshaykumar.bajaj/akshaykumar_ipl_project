//b b B N
fetch("/output/extraRunsPerTeam.json")
    .then(response => response.json())
    .then(json => {
        let teams = Object.keys(json);
        let data_array = teams.map(team => {
            return { name: team, y: json[team] }
        })

        Highcharts.chart('extraRunsPerTeam2016', {
            chart: { type: 'item' },

            title: {
                text: 'Extra Runs Per Team'
            },
            subtitle: {
                text: 'IPL Season:2016'
            },

            series: [{
                name: 'Extra Runs',
                data: data_array,
                center: ['50%', '50%'],
                size: '100%',
                startAngle: 0,
                endAngle: 360 
            }],

        })
    })
