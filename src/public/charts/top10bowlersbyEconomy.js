fetch("/output/top10bowlersbyEconomy.json")
    .then(response => response.json())
    .then(json => {
        let bowlers_array = [];
        let economy_array = [];
        json.forEach(bowler => {
            bowlers_array.push(bowler.name);
            economy_array.push(bowler.economy);
        }); 

        Highcharts.chart('top10bowlersbyEconomy2015', {
            chart: {
                type: 'bar',
            
            },
            
            title: {
                text: 'Top 10 Bowlers By Economy.'
            },
            subtitle: {
                text: 'IPL Season:2015'
            },
            xAxis: {
                title: {
                    text: 'Bowlers'
                },
                categories: bowlers_array,
            },
            yAxis: {
                title: {
                    text: 'Economy'
                },
            },
            plotOptions: {
                series: {
                    colorByPoint: true
                }
            },
            series: [{ data: economy_array ,  showInLegend: false}],
        }) 
    })
        