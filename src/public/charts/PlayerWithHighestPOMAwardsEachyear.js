//b n B N 
fetch("/output/PlayerWithHighestPOMAwards.json")
    .then(response => response.json())
    .then(json => {
        let years = Object.keys(json);
        let data_array = years.map(year => {
            return {name:year,label:json[year]}
        })
           
        Highcharts.chart('PlayerWithHighestPOMAwardsEachyear', {
            chart:{type: 'timeline'},

            title: {
                text: 'Player With Highest POM Awards'
            },
            subtitle: {
                text: 'IPL Seasons:2008-2017'
            },
            xAxis: {
                visible: false
            },
            yAxis: {
                visible: false
            },
            series: [{
                data: data_array,
            }],
            
        })
    })
