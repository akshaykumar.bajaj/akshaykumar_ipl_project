//b n B N
fetch("/output/WinningPerTeamPerYear.json")
    .then(response => response.json())
    .then(json => {
        let team_names = [];
        let years = Object.keys(json);
        let series_array = [];
        years.forEach(year => {
            let teams_played = Object.keys(json[year]);
            teams_played.forEach(team => {
                if (!team_names.includes(team))
                    team_names.push(team);
            })
        })
        team_names.sort();

        years.forEach(year => {
            let teams_played = Object.keys(json[year]);
            let wins_that_year = Object.values(json[year])
            let team_wins = [];

            team_names.forEach(team => {
                let index = teams_played.indexOf(team);
                if (index === -1)
                    team_wins.push(0);
                else
                    team_wins.push(wins_that_year[index]);
            })
            series_array.push({ type: 'column' ,name: year, data: team_wins });
        })

        Highcharts.chart('WinningPerTeamPerYearchart', {
            chart: { 
            zoomType: "x",
        },
        

            title: {
                text: 'Matches Won Per Team Per Year'
            },
            xAxis: {
                title: {
                    text: 'Year'
                },
                categories: team_names,
            },
            yAxis: {
                title: {
                    text: 'Number Of Matches Won'
                },
            },
            series: series_array,
        })
    })
