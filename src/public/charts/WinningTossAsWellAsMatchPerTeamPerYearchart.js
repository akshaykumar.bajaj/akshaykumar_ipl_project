//b n B N
fetch("/output/WinningTossAsWellAsMatchPerTeamPerYear.json")
    .then(response => response.json())
    .then(json => {
        let years = Object.keys(json);
        let team_names = [];
        let series_array = [];


        years.forEach(year => {
            let teams_played = Object.keys(json[year]);
            teams_played.forEach(team => {
                if (!team_names.includes(team)) {
                    team_names.push(team);
                }
            })
        })
        team_names.forEach(team =>{
        let data_array = [];
            years.forEach(year => {
                let teams_played = Object.keys(json[year]);
                let teams_wins = Object.values(json[year]);
                let index = teams_played.indexOf(team);
                if (index === -1) {
                    data_array.push(null);
                }
                else{
                    data_array.push(teams_wins[index]);
                }
        })
        series_array.push({name: team, data: data_array});
    })
       Highcharts.chart('WinningTossAsWellAsMatchPerTeamPerYearchart', {
            chart: {
                type: 'column',
                zoomType: "x",
            },


            title: {
                text: 'Toss as well as Matches Won Per Team Per Year'
            },
            xAxis: {
                title: {
                    text: 'Year'
                },
                categories: years,
            },
            yAxis: {
                title: {
                    text: 'Number Of Matches Won'
                },
            },
            series: series_array,
        })
    })
