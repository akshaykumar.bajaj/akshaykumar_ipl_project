fetch("/output/matchesPerYear.json")
    .then(response => response.json())
    .then(json => {
        let years = Object.keys(json);
        let data_array = years.map(year => {
            return {name:year,y:json[year]}
        })
           
        Highcharts.chart('numberofmatcheschart', {
            chart:{type: 'pie'},

            title: {
                text: 'IPL Matches Per Year'
            },
            xAxis: {
                title: {
                    text: 'Year'
                },
                categories: years
            },
            yAxis: {
                title: {
                    text: 'Number Of Matches'
                },

            },
            series: [{
                name: 'Matches played',
                data: data_array,
            }],
            
        })
    })
